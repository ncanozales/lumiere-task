import React from 'react';
import {
  useDataContext,
  useModalUpdate,
  useOpenModalUpdate,
  useSearchContext,
  useCategoryContext,
} from './DataProvider';

const Content = ({ name, home, pic, id, desc, nickname }) => {
  const setModal = useModalUpdate();
  const setModalOpen = useOpenModalUpdate();

  return (
    <div
      onClick={() => {
        setModal({ name, nickname, id, pic, desc });
        setModalOpen(true);
      }}
      className="flex flex-col justify-center items-center bg-gray-400 w-52 p-4 border border-white rounded-xl cursor-pointer"
    >
      <img src={pic} alt="Gambar" className="w-28 h-48" />
      <span className="text-base font-bold text-center text-gray-800 mt-2">
        {name}
      </span>
      <span className="text-sm font-bold text-center text-gray-600">
        {home}
      </span>
    </div>
  );
};

const Card = ({ className }) => {
  const data = useDataContext();
  const searchValue = useSearchContext();
  const category = useCategoryContext();

  return (
    <div
      className={`${className} rounded-b-xl grid sm:grid-cols-2 md:grid-cols-3 justify-center items-center bg-gray-800  gap-y-5 gap-x-5 p-4`}
    >
      {data
        ? searchValue === '' && category === ''
          ? data.map((x) => (
              <Content
                name={x.name}
                nickname={x.nickname}
                desc={x.desc}
                home={x.home}
                pic={x.thumbnail.small}
                id={x._id.$oid}
              />
            ))
          : searchValue !== '' && category !== ''
          ? data
              .filter(
                (value) =>
                  value.name.toLowerCase().includes(searchValue) &&
                  value.home === category
              )
              .map((x) => (
                <Content
                  name={x.name}
                  nickname={x.nickname}
                  desc={x.desc}
                  home={x.home}
                  pic={x.thumbnail.small}
                  id={x._id.$oid}
                />
              ))
          : searchValue !== ''
          ? data
              .filter((value) => value.name.toLowerCase().includes(searchValue))
              .map((x) => (
                <Content
                  name={x.name}
                  nickname={x.nickname}
                  desc={x.desc}
                  home={x.home}
                  pic={x.thumbnail.small}
                  id={x._id.$oid}
                />
              ))
          : data
              .filter((value) => value.home === category)
              .map((x) => (
                <Content
                  name={x.name}
                  nickname={x.nickname}
                  desc={x.desc}
                  home={x.home}
                  pic={x.thumbnail.small}
                  id={x._id.$oid}
                />
              ))
        : null}
    </div>
  );
};

export default Card;
