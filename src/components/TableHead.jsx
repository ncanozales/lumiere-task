import React from 'react';

const TableHead = () => {
  return (
    <thead className="text-xs uppercase bg-gray-700 text-gray-400">
      <tr>
        <th scope="col" className="px-4 py-3">
          No
        </th>
        <th scope="col" className="px-4 py-3">
          Informasi Karakter
        </th>
        <th scope="col" className="px-4 py-3">
          Deskripsi
        </th>
        <th scope="col" className="px-4 py-3">
          Kemampuan
        </th>
        <th scope="col" className="px-4 py-3">
          Tipe
        </th>
        <th scope="col" className="px-4 py-3">
          Home
        </th>
      </tr>
    </thead>
  );
};

export default TableHead;
