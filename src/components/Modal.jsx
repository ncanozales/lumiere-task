import React, { useEffect, useState } from 'react';
import {
  useModalContext,
  useModalUpdate,
  useOpenModalContext,
  useOpenModalUpdate,
  useDataUpdate,
  useDataContext,
} from './DataProvider';
import { removeHtmlTags } from '../utilities/removeHtmlTags';

const Modal = () => {
  const data = useDataContext();
  const setData = useDataUpdate();

  const modal = useModalContext();
  const setModal = useModalUpdate();

  const modalOpen = useOpenModalContext();
  const setModalOpen = useOpenModalUpdate();

  const [name, setName] = useState('');
  const [nickname, setNickname] = useState('');
  const [desc, setDesc] = useState('');

  useEffect(() => {
    setName(modal.name);
    setNickname(modal.nickname);
    if (typeof modal.desc === 'string') setDesc(removeHtmlTags(modal.desc));
  }, [modal]);

  return (
    <div className={`${modalOpen ? 'relative z-20' : 'hidden'}`}>
      <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
      <div className="fixed inset-0 z-10 overflow-y-auto">
        <div className="flex min-h-full items-center justify-center p-4 text-center  sm:p-0">
          <div className="relative transform overflow-hidden rounded-lg bg-gray-800 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
            {/* Bagian Atas isinya */}
            <div className="bg-gray-800 text-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4 flex justify-center items-center">
              <img
                className="h-72 w-48"
                src={modal.pic ? modal.pic : ''}
                alt="Apex Image"
              />
              <div className="py-5 w-full flex flex-col justify-center">
                <input
                  type="text"
                  value={name}
                  className="bg-gray-800 py-2 px-3 text-sm text-white border border-gray-600 rounded-lg w-full outline-none"
                  onChange={(x) => setName(x.target.value)}
                />
                <input
                  type="text"
                  value={nickname}
                  onChange={(x) => setNickname(x.target.value)}
                  className="mt-2 bg-gray-800 py-2 px-3 text-sm text-gray-400 border border-gray-600 rounded-lg w-full outline-none"
                />

                <textarea
                  cols="30"
                  rows="10"
                  value={desc}
                  onChange={(x) => setDesc(x.target.value)}
                  className="resize-none mt-2 bg-gray-800 py-2 px-3 text-sm text-gray-400 border border-gray-600 rounded-lg w-full outline-none"
                ></textarea>
              </div>
            </div>

            {/* Bagian Confirmation */}
            <div className="bg-gray-800 border-t border-gray-700 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
              <button
                onClick={() => {
                  if (modal)
                    setData((current) =>
                      current.map((obj) => {
                        if (obj._id.$oid === modal.id) {
                          return { ...obj, name, nickname, desc };
                        }

                        return obj;
                      })
                    );

                  setModalOpen(false);
                }}
                type="button"
                className="mb-2 sm:mb-0 inline-flex w-full justify-center rounded-md bg-gray-800 border border-gray-700 px-3 py-2 text-sm font-medium text-white shadow-sm hover:bg-emerald-500 sm:ml-3 sm:w-auto"
              >
                Save Changes
              </button>
              <button
                onClick={() => {
                  setModalOpen(false);
                }}
                type="button"
                className="inline-flex w-full justify-center rounded-md bg-gray-800 border border-gray-700 px-3 py-2 text-sm font-medium text-white shadow-sm hover:bg-red-400 sm:ml-3 sm:w-auto"
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Modal;
