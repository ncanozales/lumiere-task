import React, { useState } from 'react';
import { AiOutlineDown } from 'react-icons/ai';
import {
  useCategoryContext,
  useCategoryUpdate,
  useDataContext,
} from './DataProvider';
import { OutsideClick } from '../utilities/OutsideClick';

const Category = () => {
  const category = useCategoryContext();
  const setCategory = useCategoryUpdate();
  const data = useDataContext();

  const [showList, setShowList] = useState(false);

  return (
    <div className="w-44 relative">
      <div
        className="py-2 px-3 flex justify-between rounded-lg bg-white text-sm text-gray-600 cursor-pointer font-semibold"
        onClick={() => setShowList(!showList)}
      >
        {category === '' ? 'All Category' : category}
        <AiOutlineDown
          className="-mr-1 h-5 w-5 text-gray-600"
          aria-hidden="true"
        />
      </div>

      {showList ? (
        <OutsideClick onClickOutside={() => setShowList(false)}>
          <div className="z-20 absolute w-full mt-2 flex flex-col justify-between rounded-lg bg-white text-sm text-gray-500 hover:bg-gray-50 font-semibold">
            <span
              onClick={() => {
                setCategory('');
                setShowList(false);
              }}
              className="cursor-pointer p-2 border-b border-gray-300 hover:bg-gray-100 first:rounded-t-lg last:rounded-b-lg last:border-none"
            >
              All Category
            </span>
            {data &&
              data
                .map((item) => item.home)
                .filter((value, index, self) => self.indexOf(value) === index)
                .sort()
                .map((x) => (
                  <span
                    onClick={() => {
                      setCategory(x.toString());
                      setShowList(false);
                    }}
                    className="cursor-pointer p-2 border-b border-gray-300 hover:bg-gray-100 first:rounded-t-lg last:rounded-b-lg last:border-none"
                  >
                    {x}
                  </span>
                ))}
          </div>
        </OutsideClick>
      ) : null}
    </div>
  );
};

export default Category;
