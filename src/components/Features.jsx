import React from 'react';

const Features = ({ children }) => {
  return (
    <div className="flex flex-col-reverse sm:flex-row items-center justify-between py-4 bg-gray-800 px-6 rounded-3xl">
      {children}
    </div>
  );
};

export default Features;
