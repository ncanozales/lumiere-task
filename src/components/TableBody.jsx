import React from 'react';
import TableRow from './TableRow';
import {
  useSearchContext,
  useDataContext,
  useCategoryContext,
} from './DataProvider';

const TableBody = () => {
  const data = useDataContext();
  const searchValue = useSearchContext();
  const category = useCategoryContext();

  return (
    <tbody className="bg-gray-800">
      {/* <tr className="border-b bg-gray-800 border-gray-700">
        <td className="px-4 py-10 text-center pb-96" colSpan={7}>
          <span className="text-lg">Pencarian tidak ditemukan ...</span>
        </td>
        <div className="pb-96"></div>
      </tr>
      <tr className="border-b bg-gray-800 border-gray-700">
        <td className="px-4 pt-96 text-center" colSpan={7}></td>
      </tr> */}
      {data
        ? searchValue === '' && category === ''
          ? data.map((x, index) => (
              <TableRow
                name={x.name}
                nickname={x.nickname}
                type={x.type}
                desc={x.desc}
                home={x.home}
                pic={x.thumbnail.small}
                ability={x.ability}
                id={x._id.$oid}
                num={index}
              />
            ))
          : searchValue !== '' && category !== ''
          ? data
              .filter(
                (value) =>
                  value.name.toLowerCase().includes(searchValue) &&
                  value.home === category
              )
              .map((x, index) => (
                <TableRow
                  name={x.name}
                  nickname={x.nickname}
                  type={x.type}
                  desc={x.desc}
                  home={x.home}
                  pic={x.thumbnail.small}
                  ability={x.ability}
                  num={index}
                  id={x._id.$oid}
                />
              ))
          : searchValue !== ''
          ? data
              .filter((value) => value.name.toLowerCase().includes(searchValue))
              .map((x, index) => (
                <TableRow
                  name={x.name}
                  nickname={x.nickname}
                  type={x.type}
                  desc={x.desc}
                  home={x.home}
                  pic={x.thumbnail.small}
                  ability={x.ability}
                  num={index}
                  id={x._id.$oid}
                />
              ))
          : data
              .filter((value) => value.home === category)
              .map((x, index) => (
                <TableRow
                  name={x.name}
                  nickname={x.nickname}
                  type={x.type}
                  desc={x.desc}
                  home={x.home}
                  pic={x.thumbnail.small}
                  ability={x.ability}
                  num={index}
                  id={x._id.$oid}
                />
              ))
        : null}
    </tbody>
  );
};

export default TableBody;
