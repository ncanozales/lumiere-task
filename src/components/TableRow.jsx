import React from 'react';
import { limitStringLength } from '../utilities/limitStringLength';
import { removeHtmlTags } from '../utilities/removeHtmlTags';

import { useModalUpdate, useOpenModalUpdate } from './DataProvider';

const TableRow = ({
  name,
  desc,
  home,
  type,
  nickname,
  pic,
  ability,
  num,
  id,
}) => {
  const setModal = useModalUpdate();
  const setModalOpen = useOpenModalUpdate();

  return (
    <tr
      className="cursor-pointer border-b bg-gray-800 border-gray-700  hover:bg-gray-600 last:border-none"
      onClick={() => {
        setModal({ name, nickname, id, pic, desc });
        setModalOpen(true);
      }}
    >
      <td className="px-4 pt-2 text-center text-base">{num + 1}</td>

      <td className="w-72 px-4 py-2 text-white">
        <div className="flex flex-col lg:flex-row lg:justify-start justify-center items-center">
          <img className="w-24 h-32" src={pic} alt="Apex Image" />
          <div className="lg:pl-5 flex flex-col justify-center items-center lg:justify-start lg:items-start">
            <div className="text-base font-normal text-center">{name}</div>
            <div className="font-normal text-gray-500 text-center">
              {nickname}
            </div>
          </div>
        </div>
      </td>

      <td className="px-4 py-5 lg:text-justify">
        <span>{limitStringLength(removeHtmlTags(desc), 180)}</span>
      </td>

      <td className="px-4 pt-2 w-44">
        <div className="flex flex-col justify-center">
          {ability && ability.map((x) => <span>✓ {x.title}</span>)}
        </div>
      </td>
      <td className="px-4 pt-2 ">{type}</td>
      <td className="px-4 pt-2 pr-6">{home}</td>
    </tr>
  );
};

export default TableRow;
