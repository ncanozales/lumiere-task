import React from 'react';
import { useSearchContext, useSearchUpdate } from './DataProvider';

export const Search = () => {
  const searchValue = useSearchContext();
  const setSearchValue = useSearchUpdate();

  return (
    <div className="relative w-72 mb-2 sm:mb-0">
      <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
        <svg
          className="w-5 h-5 text-gray-400"
          aria-hidden="true"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
            clipRule="evenodd"
          />
        </svg>
      </div>
      <input
        value={searchValue}
        type="text"
        className="p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-full outline-none"
        placeholder="Silahkan cari karakter"
        onChange={(x) => setSearchValue(x.target.value)}
      />
    </div>
  );
};

export default Search;
