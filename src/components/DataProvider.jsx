import { useContext, useState, createContext, useEffect } from 'react';
import React from 'react';

const DataContext = createContext();
const UpdateDataContext = createContext();

const SearchContext = createContext();
const UpdateSearchContext = createContext();

const CategoryContext = createContext();
const UpdateCategoryContext = createContext();

const ModalContext = createContext();
const UpdateModalContext = createContext();

const OpenModalContext = createContext();
const UpdateOpenModalContext = createContext();

export function useDataContext() {
  return useContext(DataContext);
}
export function useDataUpdate() {
  return useContext(UpdateDataContext);
}

export function useSearchContext() {
  return useContext(SearchContext);
}
export function useSearchUpdate() {
  return useContext(UpdateSearchContext);
}

export function useCategoryContext() {
  return useContext(CategoryContext);
}
export function useCategoryUpdate() {
  return useContext(UpdateCategoryContext);
}

export function useModalContext() {
  return useContext(ModalContext);
}
export function useModalUpdate() {
  return useContext(UpdateModalContext);
}

export function useOpenModalContext() {
  return useContext(OpenModalContext);
}
export function useOpenModalUpdate() {
  return useContext(UpdateOpenModalContext);
}

const DataProvider = ({ children, className }) => {
  const [data, setData] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [category, setCategory] = useState('');
  const [modal, setModal] = useState({});
  const [modalOpen, setModalOpen] = useState(false);

  const getData = async () => {
    const res = await fetch(
      'https://raddythebrand.github.io/apex-legends/data.json'
    );
    const json = await res.json();
    setData(json);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className={className}>
      <DataContext.Provider value={data}>
        <UpdateDataContext.Provider value={setData}>
          <CategoryContext.Provider value={category}>
            <UpdateCategoryContext.Provider value={setCategory}>
              <SearchContext.Provider value={searchValue}>
                <UpdateSearchContext.Provider value={setSearchValue}>
                  <ModalContext.Provider value={modal}>
                    <UpdateModalContext.Provider value={setModal}>
                      <OpenModalContext.Provider value={modalOpen}>
                        <UpdateOpenModalContext.Provider value={setModalOpen}>
                          {children}
                        </UpdateOpenModalContext.Provider>
                      </OpenModalContext.Provider>
                    </UpdateModalContext.Provider>
                  </ModalContext.Provider>
                </UpdateSearchContext.Provider>
              </SearchContext.Provider>
            </UpdateCategoryContext.Provider>
          </CategoryContext.Provider>
        </UpdateDataContext.Provider>
      </DataContext.Provider>
    </div>
  );
};

export default DataProvider;
