export const limitStringLength = (inputString, maxLen) => {
  if (inputString.length >= maxLen) {
    return `${inputString.substring(0, maxLen)} ...`;
  }
  return inputString;
};
