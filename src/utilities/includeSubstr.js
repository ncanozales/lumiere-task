export const includeSubstr = (fullStr, subStr) => {
  if (fullStr.toLowerCase().includes(subStr)) return true;
  return false;
};
