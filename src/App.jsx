import React from 'react';

import Search from './components/Search';
import TableBody from './components/TableBody';
import Category from './components/Category';
import TableHead from './components/TableHead';
import DataProvider from './components/DataProvider';
import Features from './components/Features';
import Modal from './components/Modal';
import Card from './components/Card';

function App() {
  return (
    <DataProvider className="bg-gray-800 pt-12 pb-12 2xl:px-48 xl:px-16 px-8 min-h-screen">
      <Modal />
      <div className="w-full">
        <div className="relative lg:shadow-md  lg:rounded-t-xl lg:border lg:border-gray-500">
          <Features>
            <Category />
            <Search />
          </Features>

          <Card className="lg:hidden" />

          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 hidden lg:table">
            <TableHead />
            <TableBody />
          </table>
        </div>
      </div>
    </DataProvider>
  );
}

export default App;
