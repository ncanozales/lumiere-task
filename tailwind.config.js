/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      gridTemplateColumns: {
        1: 'repeat(1, auto)',
        2: 'repeat(2, auto)',
        3: 'repeat(3, auto)',
        4: 'repeat(4, auto)',
      },
    },
  },
  plugins: [],
};
